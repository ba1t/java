import React, {Component} from 'react';
import {connect} from "react-redux";
import { fetchQuestions, clearQuestion } from '../actions';
import {Link} from 'react-router-dom';
import _ from "lodash";
import { Segment, Grid, Button } from 'semantic-ui-react'
class Home extends Component {
    constructor(props) {
        super(props);
        this.renderQuestion = this.renderQuestion.bind(this);
    }
    componentDidMount() {
        this.props.fetchQuestions();
        this.props.clearQuestion();
    }
    renderQuestion(data) {
        return (
            <Segment key={data._id}>
                <Link to={`/question/${data._id}`}><h2>{data.question}</h2></Link>
                { data.img ?
                    <img src={data.img}/>
                 :null }
                <ul>
                {_.map(data.options, option => (
                    <li key={option._id} style={{color: option.is_true ? "red" : null}}>{option.answer}</li>
                )) }
                </ul>
            </Segment>
        )
    }
    render() {
        console.log(this.props.questions);
        return (
            this.props.questions.list.length ? 
            <Grid centered>
                <Grid.Row columns={12}>
                    <Grid.Column width={12}><Segment><Link to={`/question/${this.props.questions.list[0]._id}`} ><Button>Start</Button></Link>{!!this.props.questions.lastQuestion ? <Link to={`/question/${this.props.questions.lastQuestion}`} replace ><Button color={'yellow'}>load</Button></Link> : null } pytań: {this.props.questions.list.length}</Segment></Grid.Column>
                    <Grid.Column width={12}>
                        {_.map(this.props.questions.list , this.renderQuestion)}
                    </Grid.Column>
                </Grid.Row>
            </Grid> : null
        )
    }
}
const mapStateToProps = ({questions }) => (
    {
        questions
    }
)
export default connect(mapStateToProps, { fetchQuestions, clearQuestion  })(Home);