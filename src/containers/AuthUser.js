import React, { Component } from 'react';
import axios from 'axios';
import { withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import {authUser} from '../actions';
import { Form } from 'semantic-ui-react'
class AuthUser extends Component {
    constructor(props) {
      super(props);
      this.state = {username: '', password: ''};
  
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.renderContent = this.renderContent.bind(this);
    }
  
    handleChange(event) {
        this.setState({
          [event.target.name]: event.target.value
        })
      }

  
    handleSubmit(event) {
        event.preventDefault();
        console.log(this.state)
        const _this = this;
        axios({
        method: 'post',
        url: '//80.211.242.202:3000/login',
        data: {
            username: this.state.username,
            password: this.state.password
            }
        }).then(function(res) {
            if(res.data._id) {
                _this.props.authUser();
            }
        })
    }
    shouldComponentUpdate(nextProps) {
        return this.props.auth !== nextProps.auth
    }
    componentWillMount() {
      this.props.authUser()
    }
    renderContent() {
      switch(this.props.auth) {
        case null: 
          return <div>xxx</div>
        case false:
          return (
            <Form onSubmit={this.handleSubmit}>
              <Form.Input fluid onChange={this.handleChange} value={this.state.username} label='username' name='username' placeholder='Name Name' />
              <Form.Input fluid type="password" onChange={this.handleChange} value={this.state.password} label='password' name='password' placeholder='password Name' />
              <Form.Button>Submit</Form.Button>
            </Form>
          )
        default:
          return <Redirect to='/' />
      }
    }
    render() {
      return (
        this.renderContent()
      );
    }
  }

const mapStateToProsp = ({auth}) => (
    {auth}
  ) 

export default connect(mapStateToProsp, {authUser})(withRouter(AuthUser));