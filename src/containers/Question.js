 import React, {Component} from 'react';
import { Checkbox, Grid, Form, Button, Icon, Segment, Label } from 'semantic-ui-react';
import { connect } from 'react-redux';
import {fetchQuestions, getQuestion, clearQuestion, random, saveQuestion} from '../actions';
import { Link } from 'react-router-dom'; 
import _ from 'lodash';
class Question extends Component {
    constructor(props) {
        super(props);
        this.state = {
            check: false
        };
        this.handleChange =this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.random =this.random.bind(this);
        this.renderOptions = this.renderOptions.bind(this);
    }
    componentDidMount() {
        this.props.fetchQuestions();
        this.props.getQuestion(this.props.match.params.id, this.props.questions.random);
    }
    componentWillUnmount() {
        this.props.clearQuestion()
    }
    componentDidUpdate(prevProps) {
        if(this.props.match.params.id !== prevProps.match.params.id) {
            this.props.getQuestion(this.props.match.params.id, this.props.questions.random);
            this.setState({check: false})
        }
        if(this.props.questions.random !== prevProps.questions.random) {
            this.props.getQuestion(this.props.match.params.id, this.props.questions.random);
        }

    }
    
    handleChange = (e, { checked, key }) => {
            const obj = {}
            obj[key] = checked;
            this.setState({...this.state, ...obj});           
    }
    handleSubmit(e) {
    }
    random() {
            const randQuestion = _.random(0, this.props.questions.list.length -1);
            if(this.props.questions.list[randQuestion]._id !== this.props.match.params.id){
                return this.props.questions.list[randQuestion]._id;
            } else {
                return this.props.match.params.id
            }
    }
    renderOptions(option) {
        return <Form.Field style={this.state.check && option.is_true ? {border:  "2px solid LimeGreen", padding: "5px", width: "100%"} : {padding: "5px", width: "100%", border: "2px solid transparent"}}toggle control={Checkbox} key={option._id}  label={{ children: option.answer }} />
    }
    render() {
        return (
            this.props.questions.current ?
            <Grid padded={'horizonta'} centered>
                <Grid.Row columns={12}>
                <Grid.Column width={12}>
                <Segment.Group>
                        <Segment padded>                       
                        <Link to="/"><Button>Home</Button></Link><Button color={this.props.questions.random? 'green' : 'red'} onClick={()=>this.props.random(!this.props.questions.random)}>Random</Button>
                        <Button color={'purple'} onClick={()=>this.props.saveQuestion(this.props.match.params.id)}>save</Button>
                        {!!this.props.questions.lastQuestion ? <Link to={`/question/${this.props.questions.lastQuestion}`} replace ><Button color={'yellow'}>load</Button></Link> : null }
                        </Segment>
                        <Segment>      
                            
                                <h3><Label color="black" size={'big'} horizontal>{this.props.questions.current.count}/{this.props.questions.list.length}</Label> {this.props.questions.current.question}</h3></Segment>
                    { this.props.questions.current.img ?
                        <Segment><img src={this.props.questions.current.img}/></Segment>
                     : null }
                    <Segment>
                        <Form onSubmit={this.handleSubmit}>
                            {_.map(this.props.questions.current.options, this.renderOptions)}
                        </Form>
                        </Segment>
                    <Segment>
                        {this.props.questions.current.prev ? <Link to={`/question/${this.props.questions.current.prev}`} replace ><Button icon labelPosition='left'>
                        <Icon name='left arrow' />
                        Prev
                        </Button></Link> : null }
                        <Link to={`/question/${this.random()}`}><Button icon labelPosition='right'>
                        Random
                        <Icon name='random' />
                        </Button></Link>
                        {!this.state.check ? <Button  color={'green'} onClick={()=>this.setState({check: true})}>Sprawdź</Button> : <Button disabled >Sprawdź</Button> }
                        {this.props.questions.current.next ? <Link to={`/question/${this.props.questions.current.next}`} replace ><Button icon labelPosition='right'> 
                        Next
                        <Icon name='right arrow' />
                        </Button></Link> : null }
                    </Segment>
                    </Segment.Group>
                    </Grid.Column>
                </Grid.Row> 
            </Grid>: null

        )
    }
}
const mapStateToProps = ({questions}) => (
    {
        questions
    }
)
export default connect(mapStateToProps, { fetchQuestions, getQuestion ,clearQuestion, random, saveQuestion })(Question);