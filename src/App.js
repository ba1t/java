import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import Routes from './Routes';
import rootReducer from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import { loadState, saveState } from './helpers/localStorage';

const persistedState = loadState();
const store = createStore(
    rootReducer, 
    persistedState,
    composeWithDevTools(
    applyMiddleware(reduxThunk),
  ));

store.subscribe(() => {
    saveState({...store.getState() });
});
const App = () => (
    <Provider store={store}>
        <Routes />
    </Provider>
)

export default App;





