import {FETCH_QUESTIONS, GET_QUESTION_BY_ID, CLEAR_QUESTION, RANDOM, SAVE_QUESTION} from '../actions/types';
import _ from 'lodash';

export default function(state = {list: [], question: {}, random: false, lastQuestion: null}, action) {
    switch (action.type) {
        case FETCH_QUESTIONS:
            return { ...state, list: [...action.payload], }
        case GET_QUESTION_BY_ID:
            const newState = [...state.list];
            const count = _.findIndex(newState, ['_id', action.payload.current._id]) + 1;
            if(action.payload.shuffle) {
                const options = _.shuffle(action.payload.current['options'])
                return {...state, current: {...action.payload.current, options, count}}
            } 
            return {...state, current: {... action.payload.current, count}}
        case CLEAR_QUESTION:
            return {
                ...state,
                current: null,
            }
        case RANDOM:
            return {
                ...state,
                random: action.payload
            }
        case SAVE_QUESTION:
            return {
                ...state,
                lastQuestion: action.payload        
            }
        default:
            return state;
    }
}