import questions from './reducerQuestion';
import auth from './reducerAuth'

import { combineReducers } from 'redux';

const rootReducer =  combineReducers({
    auth,
    questions
})

export default rootReducer;