import React, { Component } from 'react';
import { BrowserRouter, Switch,  Route, Redirect } from 'react-router-dom';
import _ from 'lodash';
import { connect } from 'react-redux';
import Home from './containers/Home';
import {authUser} from './actions'
import Question from './containers/Question';
import AuthUser from './containers/AuthUser';

function PrivateRoute ({component: Component, authenticated, ...rest}) {
    return (
      <Route exact
        {...rest}
        render={(props) => authenticated
          ? <Component {...props} />
          : <Redirect to='/login' />}
      />
    )
}

class Routes extends Component {
    componentWillMount() {
        this.props.authUser();
    }
    render() {
        return (    
                <BrowserRouter>
                    <Switch>
                        <PrivateRoute authenticated={this.props.auth} path='/add/question' component={Question} />
                        <PrivateRoute authenticated={this.props.auth} path='/question/:id/edit' compoent={Question} />
                        <Route exact path='/' component={Home}/>
                        <Route exact path='/question/:id' component={Question}/>
                        <Route exact path='/login' component={AuthUser} />
                        <Redirect to="/" />
                    </Switch>
                </BrowserRouter>
            
        )
    }
}

const mapStateToProps  = ({auth}) => (
    {auth}
)
export default connect( mapStateToProps, { authUser } )(Routes);

