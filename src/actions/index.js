import {FETCH_QUESTIONS, CLEAR_QUESTION, GET_QUESTION_BY_ID, RANDOM, SAVE_QUESTION, AUTH_USER} from './types';
import axios from 'axios'

export const authUser = () => async dispatch => {
    const res = await axios.get('http://80.211.242.202:3000/profile');
    dispatch({type: AUTH_USER, payload: res.data})
}
export const fetchQuestions = () => async dispatch => {
    const res = await axios.get('http://80.211.242.202:3000/api/questions');
    dispatch({type: FETCH_QUESTIONS, payload: res.data});
}
export const getQuestion = (id, shuffle) => async dispatch => {
    const res = await axios.get(`http://80.211.242.202:3000/api/questions/${id}`);
    dispatch({type: GET_QUESTION_BY_ID, payload: {current: res.data, shuffle: shuffle}});
}

export const random = (random) => async dispatch => {
    dispatch({type: RANDOM, payload: random });
}

export const clearQuestion = () => dispatch => {
    dispatch({type: CLEAR_QUESTION})
}

export const saveQuestion = (id) => dispatch => {
    dispatch({type: SAVE_QUESTION, payload: id})
}
