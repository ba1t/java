export const GET_QUESTION_BY_ID = "get question by id";
export const FETCH_QUESTIONS = "get all questions";
export const CLEAR_QUESTION = "clear";
export const RANDOM = "random";
export const SAVE_QUESTION = "save";
export const AUTH_USER = 'auth user'